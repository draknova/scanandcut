FROM python:3.7

RUN apt update
RUN apt install -y potrace libopencv-dev


WORKDIR /usr/src/app

COPY dist/scanandcut-*.tar.gz ./
RUN pip install scanandcut-*

CMD ["scanandcut-ui"]
