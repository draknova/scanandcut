# Scan&Cut

Scan&Cut is a utility tool to easily generate SVG files from pictures and use them with your Cutting Printer.

## Screenshot

![Select Image](img/select_image.png)

## Installation

There is few way to install the tool:

- [docker](#)
- [docker-compose](#)
- [pip](#)

## Usage
Scan&Cut can be executed in 3 different way:

- [WebUI](./usage/) - A mobile friendly UI
- [Commandline](#) - For advanced user
- [API](#) - For python user


## Support
If you need help or encounter a bug, please create a ticket on [GitLab](https://gitlab.com/draknova/scanandcut/-/issues/new).

## Roadmap

- Improve page detection
- Improve performance
- Batch processing
- History
- Ability to print out straigh away from your printer (might not be possible)

## Contributing
Feel free to contribute to this project by doing Merge Request.

## Author
- Sebastien Marsais

## License
This product is licensed under the [MIT License](https://opensource.org/licenses/MIT).

## Project status
This project is still under active development. Though I cannot dedicate my fulltime to it and therefor it might take time to fix or implement new feature.
