## Docker

Follow instruction on how to install [docker](https://docs.docker.com/engine/).

```
docker run ...
```

## Docker Compose

Follow instruction on how to install [docker-compose](https://docs.docker.com/compose/install/).
```
docker-compose up
```

## Pip

```
pip install ...
```

Requirements:

- python>3.4
- potrace
