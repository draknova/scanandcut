# Usage

## Connect to WebUI
Once you have started the WebUI (depending on your installation), use your web browser on your computer or phone to connect to the host computer:

```bash
http://<ip>:<port>
```

So if you are running it locally, default should be:

```bash
http://localhost:8080
```


## Uploading Scan

![Select Image](img/select_image.png)

Click the __Browse...__ button to select or take a picture of the element you want to scan.

Then click __Upload__ button to move to the next step.


## Creating Cutting Mask

This page is divided in 4 sections:

- Page Properties
- Page Identification
- Threshold
- Mask

Each section should be filled and submitted one after an other to ensure the process is moving smoothly.


### Page Properties

![General Screenshot](img/general.png)

This section is used to gather important informations regarding the page we are going to manipulate. You are then requested to fill those two parameter

- __Height__ - Vertical dimension of the page to cutout in millimeters.
- __Width__ - Horizontal dimension of the page to cutout in millimeters.

The others settings don't really required to be changed.

- __DPI__ - Number of pixels per inch for generated intermediate images. Use this parameter to scale up/down generated image resolutions. Low value will mean lower resolution but faster computing time where as higher value will generate more precise images.


### Page Identification

![Page Identification Screenshot](img/page_identification.png)

In this section short section is used to identify the page to cutout in the given picture. It only contain a single setting:

- __Background Margin__ - Size in percent of the pictures edges used to identify background. If you set this value to 0, this step will be ignored - this is the case if you scanned the page with a scanner.

The image presented on the left would have two shape drawn:

- __Green__ represent the identified page. If not visible, that's mean the page was not be identified properly.
- __Red__ represent the pictures edges used to identify the background. It is controlled by the parameter __Background Margin__.

### Threshold

![Threshold Screenshot](img/threshold.png)

This step is generating a black and white mask that will be used to identify shapes in the next Mask step.

- __Threshold Mode__ - Black & White convertion mode. Changing this value might provide better results depending on the subject to identify.
    - Gray - Generic black and white convertion
    - Hue - Use this mode if there is high color hue contrast (example: Blue over red)
    - Saturation - Use this mode if there is high color staturation contrast (example: Red over pink)
    - Value - Use this mode if there is high color brightness contrast (example: Bright red over dark red)
    - Red - Use Red channel
    - Green - Use Green channel
    - Blue - Use Blue channel
- __Line Offset__ - Dilate/Shrink black pattern. Changing this value can help closing small gaps in close shapes.
- __Threshold Radius__ - Size in mm of the area to identify contrasting pixels. Smaller values willincrease details but increase noise. This parameter is highly linked to _Threshold Weight_.
- __Threshold Weight__ - Amount of contrast required. Decrease this value if the constrast between the shapes and the page is low.

### Masks

![Mask Screenshot](img/mask.png)

In this last step you are configuring the cutting mask you will be exporting.

- __Trace Mode__ - Set if you want to export fill shape or just cutting lines
    - Fill - Shapes are considered full and no cutting lines will be generated inside
    - Stroke - Shapes may have holes and internal cutting lines will be generated
- __Offset__ - Add a margin between the shape and the cuting lines.
- __Trace Simplify__ - Reduce the number of point in the generated curves. The lower the value is, the more points are kept.
- __Trace Width__ - (Stroke Mode only) Stroke display size
- __Pattern Max Size__ - Maximum area in page percentage that a shape must cover.
- __Pattern Min Size__ - Minimum area in page percentage that a shape must cover.
- __Tracing Min Level__ - Minimum level of the top parent cuting line.
- __Tracing Max Level__ - Maximum level of the parent cuting line.
- __Corner Size__ - Set the size in millimeters of the page corner marks.
- __Corner Thickness__ - Set the thickness in millimeters of the page corner marks.

The first image on the left is the generated cutting mask. Colors are only here to help identifying the different shapes.
The second image is the "Validation Mask" and help you identify if the cutting mask will be cutting properly every elements you want.

## Download Mask
Once you are happy with all the settings, press __Download Mask__ to download the cutting mask.

The next steps depends on your cutting machine and won't be described on this page.
