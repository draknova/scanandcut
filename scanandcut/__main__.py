import argparse
from .api import Settings, run
import sys


def main():
    settings = Settings()

    parser = argparse.ArgumentParser()
    parser.add_argument("input_file", help="Image file")
    parser.add_argument(
        "--height", type=float, help="Paper size height", required=True
    )
    parser.add_argument(
        "--width", type=float, help="Paper size width", required=True
    )
    parser.add_argument(
        "--dpi",
        type=float,
        help="Paper size width",
    )
    parser.add_argument(
        "--margin",
        help="Margin in the image to determine isolate background color",
    )
    parser.add_argument(
        "--line-offset",
        type=float,
        help="Paper size width",
    )
    parser.add_argument(
        "--threshold-radius",
        type=float,
        help="Paper size width",
    )
    parser.add_argument(
        "--threshold-weight",
        type=float,
        help="Paper size width",
    )
    parser.add_argument(
        "--max-size",
        type=float,
        help="Paper size width",
    )
    parser.add_argument(
        "--min-size",
        type=float,
        help="Paper size width",
    )
    parser.add_argument(
        "--offset",
        type=float,
        help="Paper size width",
    )
    parser.add_argument(
        "--corner-length",
        type=float,
        help="Paper size width",
    )
    parser.add_argument(
        "--corner-width",
        type=float,
        help="Paper size width",
    )

    parser.add_argument("--output-file", help="Image file")
    parser.add_argument("--tmp-dir")
    args = parser.parse_args()

    for key, value in args._get_kwargs():
        if hasattr(settings, key) and value is not None:
            setattr(settings, key, value)
    settings.threshold_radius = 20
    settings.threshold_weight = 20

    output_file = args.output_file or args.input_file + ".svg"
    run(args.input_file, output_file, settings)


if __name__ == "__main__":
    sys.exit(main())
