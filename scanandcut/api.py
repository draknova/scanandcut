import cairo
import cv2 as cv
from enum import Enum, auto
import numpy as np
from PIL import Image
import os
import logging
import math

import shapely
import time

logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger(__name__)


CM_TO_INCH = 0.393701


class Unit(Enum):
    MM = "mm"
    CM = "cm"
    IN = "in"


class ThresholdMode(Enum):
    GRAY = auto()
    HUE = auto()
    SATURATION = auto()
    VALUE = auto()
    RED = auto()
    GREEN = auto()
    BLUE = auto()


class TraceMode(Enum):
    FILL = auto()
    STROKE = auto()


class Settings:
    """Configure Scan&Cut operation."""

    def __init__(self, **kwargs):
        self.width = None
        self.height = None
        self.unit = Unit.MM
        self.dpi = 300

        # Page identification
        self.margin = 0.02

        # Pattern identification
        self.threshold_mode = ThresholdMode.GRAY
        self.line_offset = 1
        self.threshold_radius = 5
        self.threshold_weight = 50

        # Tracing
        self.trace_mode = TraceMode.FILL
        self.max_size = 0.95
        self.min_size = 0.005
        self.offset = 1
        self.trace_simplify = 1.5
        self.trace_width = 1
        self.corner_length = 5
        self.corner_width = 1
        self.min_level = 1
        self.max_level = 2

        self.tmp_dir = None
        self.write_tmp_images = True

        for key, value in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, value)

    def __setattr__(self, name, value):
        """Set an attribute."""
        if name == "unit":
            if not isinstance(value, Unit):
                value = Unit._member_map_[value.upper()]
        if name == "threshold_mode":
            if not isinstance(value, ThresholdMode):
                value = [
                    mode
                    for mode in ThresholdMode.__members__.values()
                    if mode.value == value
                ][0]
        if name == "trace_mode":
            if not isinstance(value, TraceMode):
                value = [
                    mode
                    for mode in TraceMode.__members__.values()
                    if mode.value == value
                ][0]
        return super().__setattr__(name, value)

    def __str__(self):
        """Pretty print settings."""
        return (
            "Settings{\n\t"
            + "\n\t".join(
                ["{}={}".format(k, v) for k, v in self.__dict__.items()]
            )
            + "\n}"
        )


def timer(foo):
    def __wrapper__(*args, **kwargs):
        start = time.time()
        result = foo(*args, **kwargs)
        print("{} - {:.4f}s".format(foo.__name__, time.time() - start))
        return result

    return __wrapper__


_CACHE = {}


def cached(*keys):
    def __foo_wrapper__(foo):
        def __wrapper__(*args, **kwargs):
            cache_key = [foo]
            for arg in args + tuple(kwargs.values()):
                if isinstance(arg, Settings):
                    cache_key += [str(getattr(arg, key)) for key in keys]
                # else:
                #    cache_key.append(str(arg))
            key = tuple(cache_key)
            if key not in _CACHE:
                _CACHE[key] = foo(*args, **kwargs)
            return _CACHE[key]

        return __wrapper__

    return __foo_wrapper__


def _write_tmp_image(image, name, settings):
    """Write out image to temporary folder."""
    if settings.write_tmp_images and settings.tmp_dir:
        if not os.path.isdir(settings.tmp_dir):
            os.makedirs(settings.tmp_dir)
        Image.fromarray(image).save(os.path.join(settings.tmp_dir, name))


def to_pixel(value, settings: Settings):
    """Convert value to pixel."""
    if settings.unit is Unit.MM:
        value *= 0.1
    if settings.unit in (Unit.CM, Unit.MM):
        value = value * CM_TO_INCH
    return int(value * settings.dpi)


def get_tan(i, pts):
    if i < 0:
        i = len(pts) - 1 + i
    prev = np.array(pts[i - 1])
    cur = np.array(pts[i])
    next = np.array(pts[1 if i + 1 >= len(pts) else i + 1])

    v1 = cur - prev
    v1_mag = np.sqrt(v1.dot(v1))
    v2 = next - cur
    v2_mag = np.sqrt(v2.dot(v2))

    # Tangent
    t = prev - next
    mag = np.sqrt(t.dot(t))

    ratio = 0.2
    t1 = t * ratio / (mag or 1.0) * min(v1_mag, v2_mag)
    t2 = t * ratio / (mag or 1.0) * v1_mag
    t3 = t * ratio / (mag or 1.0) * v2_mag
    return t1, t2, t3


class CV_Contour:
    def __init__(self, id, pts, parent=None):
        pts = pts[:, 0]
        # pts = smooth(np.array(pts[:, 0], dtype=np.float64))
        self.id = id
        self.pts = pts
        self._parent = parent
        self._children = []

        if self.parent:
            self.parent.add_child(self)

    def __repr__(self):
        return "<level={} pts={}>".format(self.level, len(self.pts))

    @property
    def children(self):
        return tuple(self._children)

    @property
    def parent(self):
        return self._parent

    @property
    def level(self):
        if not self.parent:
            return 0
        return self.parent.level + 1

    def add_child(self, child):
        self._children.append(child)

    def trace_contour(self, context, settings):
        if (
            self.level > settings.max_level
            or settings.trace_mode is TraceMode.FILL
            and self.level > settings.min_level
        ):
            return
        if self.level >= settings.min_level:
            self._trace_contour(context, settings)

        for child in self.children:
            child.trace_contour(context, settings)

    def _trace_contour(self, context, settings):
        if len(self.pts) < 4:
            return

        line = shapely.LinearRing(self.pts)
        if settings.offset:
            line = line.offset_curve(
                -to_pixel(settings.offset, settings), quad_segs=4
            )
        if not len(line.coords):
            return
        line = line.simplify(settings.trace_simplify, True)
        # line = shapely.remove_repeated_points(line, tolerance=5)
        # line = line.simplify(tolerance=4)

        # The color of the shape
        np.random.seed(self.id)
        color = list(
            int(n) / 255.0 for n in np.random.choice(range(255), size=3)
        )

        context.new_path()
        context.move_to(line.coords[0][0], line.coords[0][1])
        context.set_source_rgba(color[0], color[1], color[2], 1)
        context.set_line_width(to_pixel(settings.trace_width, settings))
        for i, pt in enumerate(line.coords):
            if i >= len(line.coords) - 1:
                break
            next_i = 0 if i + 1 >= len(line.coords) else i + 1
            cur_pt = np.array(line.coords[i])
            next_pt = np.array(line.coords[next_i])
            x0, y0 = cur_pt
            x1, y1 = cur_pt - get_tan(i, line.coords)[2]
            x2, y2 = next_pt + get_tan(next_i, line.coords)[1]
            x3, y3 = next_pt
            context.curve_to(x1, y1, x2, y2, x3, y3)
        context.close_path()
        if settings.trace_mode is TraceMode.FILL:
            context.fill()
        else:
            context.stroke()

        return
        for i, pt in enumerate(line.coords):
            context.set_line_width(2)
            context.set_source_rgba(255, 0, 0, 1)
            x, y = cur_pt = np.array(pt)
            x1, y1 = cur_pt - get_tan(i, line.coords)[2]
            x2, y2 = cur_pt + get_tan(i, line.coords)[1]
            context.arc(x, y, 2, 0, 2 * math.pi)
            context.fill()
            context.move_to(x1, y1)
            context.line_to(x2, y2)
            context.stroke()


@timer
@cached("margin")
def identify_page(image, settings: Settings):
    """Identify page by generating mask."""
    LOGGER.info("Identifying page...")
    blur = cv.bilateralFilter(image, 9, 75, 75)

    # Image margin where to get masking colors from
    height, width, _ = image.shape
    LOGGER.info("Original image resolution: %i/%i", height, width)

    margin = settings.margin
    if not margin:
        return image, ((0, 0), (width, 0), (width, height), (0, height))
    top = right = bottom = left = margin

    # Calculate margin
    top = max(1, int(top * height))
    bottom = -max(1, int(bottom * height))
    left = max(1, int(left * width))
    right = -max(1, int(right * width))
    LOGGER.info("Margin: %i, %i, %i, %i", top, right, bottom, left)

    # Get colors from margin
    a1 = blur[:top].reshape(-1, 3)
    a2 = blur[bottom:].reshape(-1, 3)
    a3 = blur[:, :left].reshape(-1, 3)
    a4 = blur[:, right:].reshape(-1, 3)
    colors = np.unique(np.concatenate((a1, a2, a3, a4)), axis=0)

    # Generate mask
    page_mask = np.array(
        np.all(
            np.logical_and(
                np.min(colors, axis=0) < blur, blur < np.max(colors, axis=0)
            ),
            axis=-1,
        ),
        dtype=np.uint8,
    )
    page_contours = identify_contour(page_mask)
    page_contours_image = image.copy()
    cv.drawContours(
        page_contours_image,
        [page_contours],
        0,
        (0, 255, 0),
        max(1, int(min(width, height) * 0.005)),
    )
    cv.rectangle(
        page_contours_image,
        (left, top),
        (width + right, height + bottom),
        (255, 0, 0),
        max(1, int(min(width, height) * 0.005)),
    )
    _write_tmp_image(page_contours_image, "page_contours.jpg", settings)
    return page_mask, page_contours


@timer
def identify_contour(page_mask):
    """Return list of contours points."""
    LOGGER.info("Identifying page contour...")
    height, width = page_mask.shape
    contours, hierarchy = cv.findContours(page_mask, 1, 2)
    for cnt in sorted(contours, key=lambda c: cv.contourArea(c), reverse=True):
        if cv.contourArea(cnt) >= height * width * 0.99:
            continue
        if cv.contourArea(cnt) < height * width * 0.1:
            continue
        epsilon = 0.1 * cv.arcLength(cnt, True)
        approx = cv.approxPolyDP(cnt, epsilon, True)

        if len(approx) == 4:
            print("Found")
            break
    return approx


@timer
def distord(ref_img, page_contours, settings):
    """Undistord image."""
    LOGGER.info("Distording page...")
    tmp_img = ref_img.copy()
    height, width, _ = ref_img.shape

    # Calculate page resolution
    res_x = to_pixel(settings.height, settings)
    res_y = to_pixel(settings.width, settings)
    LOGGER.info("Undistorded page resolution: %i/%i", res_x, res_y)

    if res_x > 5000 or res_y > 5000:
        raise RuntimeError("Too big!!!")

    # Identify points from contour based on their proximity to each corners
    bottom_left = sorted(
        page_contours,
        key=lambda a: np.linalg.norm(np.array([height, 0]) - np.array(a)),
    )[0]
    bottom_right = sorted(
        page_contours,
        key=lambda a: np.linalg.norm(np.array([height, width]) - np.array(a)),
    )[0]
    top_left = sorted(
        page_contours,
        key=lambda a: np.linalg.norm(np.array([0, 0]) - np.array(a)),
    )[0]
    top_right = sorted(
        page_contours,
        key=lambda a: np.linalg.norm(np.array([0, width]) - np.array(a)),
    )[0]

    # Distord image
    pts1 = np.float32([top_left, top_right, bottom_left, bottom_right])
    pts2 = np.float32([[0, 0], [0, res_x], [res_y, 0], [res_y, res_x]])
    M = cv.getPerspectiveTransform(pts1, pts2)

    undistorded_image = cv.warpPerspective(tmp_img, M, (res_y, res_x))
    return undistorded_image


@cached(
    "threshold_mode", "threshold_radius", "threshold_weight", "line_offset"
)
def threshold(ref_img, settings):
    # Blur image
    tmp_img = ref_img.copy()
    tmp_img = cv.bilateralFilter(tmp_img, 9, 75, 75)

    # Convert to gray more
    if settings.threshold_mode is ThresholdMode.GRAY:
        gray = cv.cvtColor(tmp_img, cv.COLOR_RGB2GRAY)
    elif settings.threshold_mode is ThresholdMode.HUE:
        gray, _, _ = cv.split(cv.cvtColor(tmp_img, cv.COLOR_RGB2HSV))
    elif settings.threshold_mode is ThresholdMode.SATURATION:
        _, gray, _ = cv.split(cv.cvtColor(tmp_img, cv.COLOR_RGB2HSV))
    elif settings.threshold_mode is ThresholdMode.VALUE:
        _, _, gray = cv.split(cv.cvtColor(tmp_img, cv.COLOR_RGB2HSV))
    elif settings.threshold_mode is ThresholdMode.RED:
        gray, _, _ = cv.split(tmp_img)
    elif settings.threshold_mode is ThresholdMode.GREEN:
        _, gray, _ = cv.split(tmp_img)
    elif settings.threshold_mode is ThresholdMode.BLUE:
        _, _, gray = cv.split(tmp_img)

    # Generate threshold image
    threshold_radius = max(1, to_pixel(settings.threshold_radius, settings))
    threshold_weight = settings.threshold_weight
    if threshold_radius % 2 == 0:
        threshold_radius += 1

    gray = cv.adaptiveThreshold(
        gray,
        255,
        cv.ADAPTIVE_THRESH_GAUSSIAN_C,
        cv.THRESH_BINARY,
        threshold_radius,
        threshold_weight,
    )

    # Dilate pattern for better identification of contours
    line_offset = to_pixel(settings.line_offset, settings)
    if line_offset:
        LOGGER.info("Dilating shapes %i...", line_offset)
        kernel = cv.getStructuringElement(
            cv.MORPH_ELLIPSE, (line_offset * 2, line_offset * 2)
        )
        gray = cv.erode(gray, kernel, iterations=1)
    _write_tmp_image(gray, "threshold.jpg", settings)
    return gray


@timer
def identify_stamps(threshold_img, settings):
    """Identify stamps in image."""
    LOGGER.info("Identifying shapes contours...")
    contours, hierarchy = cv.findContours(
        threshold_img, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE
    )

    # Creating hiearchy group
    cv_contours = []
    for i, h in enumerate(np.reshape(hierarchy, (-1, 4))):
        parent = None
        if h[3] != -1:
            parent = cv_contours[h[3]]
        cv_contours.append(CV_Contour(i, contours[i], parent=parent))

    LOGGER.info("Drawing masks...")
    mask = np.full(
        (threshold_img.shape[0], threshold_img.shape[1]), 255, dtype=np.uint8
    )
    with cairo.SVGSurface(
        os.path.join(settings.tmp_dir, "stamps.svg"),
        to_pixel(settings.width, settings),
        to_pixel(settings.height, settings),
    ) as surface:
        context = cairo.Context(surface)
        context.scale(1, 1)

        mark_corners(context, settings)

        for cv_contour in cv_contours:
            if cv_contour.level != 0:
                continue
            cv_contour.trace_contour(context, settings)

        mask = np.ndarray(
            shape=(
                to_pixel(settings.height, settings),
                to_pixel(settings.width, settings),
                4,
            ),
            dtype=np.uint8,
            buffer=surface.map_to_image(None).get_data(),
        )
        mask = mask[:, :, 3]
    _write_tmp_image(mask, "mask.jpg", settings)
    return mask


def validation_mask(ref_img, mask, settings):
    validation_mask = cv.bitwise_or(ref_img.copy(), ref_img.copy(), mask=mask)
    _write_tmp_image(validation_mask, "validation_mask.jpg", settings)
    return mask


@timer
def mark_corners(context, settings):
    LOGGER.info("Marking page corner...")
    length = to_pixel(settings.corner_length, settings)
    thickness = to_pixel(settings.corner_width / 2, settings)
    height = to_pixel(settings.height, settings)
    width = to_pixel(settings.width, settings)

    context.set_line_width(thickness * 2)

    # Top left
    context.new_path()
    context.move_to(thickness, length)
    context.line_to(thickness, thickness)
    context.line_to(length, thickness)
    context.stroke()

    # Top right
    context.new_path()
    context.move_to(width - length, thickness)
    context.line_to(width - thickness, thickness)
    context.line_to(width - thickness, length)
    context.stroke()

    # Bottom right
    context.new_path()
    context.move_to(width - thickness, height - length)
    context.line_to(width - thickness, height - thickness)
    context.line_to(width - length, height - thickness)
    context.stroke()

    # Bottom left
    context.new_path()
    context.move_to(length, height - thickness)
    context.line_to(thickness, height - thickness)
    context.line_to(thickness, height - length)
    context.stroke()


@timer
def mark_corners2(mask, settings):
    """Add small mark in page image corners."""
    LOGGER.info("Marking page corner...")
    length = to_pixel(settings.corner_length, settings)
    thickness = to_pixel(settings.corner_width, settings)
    height, width = mask.shape
    # Top left
    mask = cv.rectangle(mask, (0, 0), (length, thickness), (0, 0, 0), -1)
    mask = cv.rectangle(mask, (0, 0), (thickness, length), (0, 0, 0), -1)

    # Bottom left
    mask = cv.rectangle(
        mask, (0, height - thickness), (length, height), (0, 0, 0), -1
    )
    mask = cv.rectangle(
        mask, (0, height - length), (thickness, height), (0, 0, 0), -1
    )

    # Bottom right
    mask = cv.rectangle(
        mask,
        (width - length, height - thickness),
        (width, height),
        (0, 0, 0),
        -1,
    )
    mask = cv.rectangle(
        mask,
        (width - thickness, height - length),
        (width, height),
        (0, 0, 0),
        -1,
    )

    # Top right
    mask = cv.rectangle(
        mask, (width - length, 0), (width, thickness), (0, 0, 0), -1
    )
    mask = cv.rectangle(
        mask, (width - thickness, 0), (width, length), (0, 0, 0), -1
    )
    return mask


def run(in_filepath, out_filepath, settings):
    img = cv.imread(in_filepath)
    if img is None:
        return False
    image = cv.cvtColor(img, cv.COLOR_BGR2RGB)
    page_mask, page_contour = identify_page(image, settings)
    undistorded_image = distord(image, page_contour, settings)
    threshold_img = threshold(undistorded_image, settings)
    mask = identify_stamps(threshold_img, settings)
    validation_mask(undistorded_image, mask, settings)
    return True
