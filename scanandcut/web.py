from starlette.applications import Starlette
from starlette.responses import RedirectResponse
from starlette.templating import Jinja2Templates
from starlette.routing import Route, Mount
from starlette.endpoints import HTTPEndpoint
from starlette.staticfiles import StaticFiles
import os
import uvicorn
import sys

from scanandcut.api import Settings, ThresholdMode, TraceMode, run
import tempfile


templates = Jinja2Templates(
    directory=os.path.join(os.path.dirname(__file__), "templates")
)


TMP_DIR = "/tmp/scanandcut"


class Upload(HTTPEndpoint):
    async def post(self, request):
        dirpath = tempfile.mkdtemp(dir=TMP_DIR)
        async with request.form(max_files=1000, max_fields=1000) as form:
            contents = await form["image"].read()
            filepath = os.path.join(dirpath, "source.jpg")
            with open(filepath, "wb") as fp:
                fp.write(contents)
        return RedirectResponse(url=f"/edit/{os.path.basename(dirpath)}/")


class Edit(HTTPEndpoint):
    async def post(self, request):
        return await self.get(request)

    async def get(self, request):
        settings = Settings()
        for key, value in request.query_params.items():
            if key == "unit":
                continue
            if hasattr(settings, key):
                try:
                    if key == "trace_fill":
                        value = bool(value)
                    else:
                        value = float(value)
                except TypeError:
                    pass
                setattr(settings, key, value)
        settings.tmp_dir = os.path.join(TMP_DIR, request.path_params["path"])

        if settings.width and settings.height:
            run(
                os.path.join(
                    TMP_DIR, request.path_params["path"], "source.jpg"
                ),
                os.path.join(
                    TMP_DIR, request.path_params["path"], "result.svg"
                ),
                settings=settings,
            )

        return templates.TemplateResponse(
            "edit.html",
            {
                "request": request,
                "settings": settings,
                "path": request.path_params["path"],
                "ThresholdMode": ThresholdMode,
                "TraceMode": TraceMode,
            },
        )


async def home(request):
    return templates.TemplateResponse(
        "index.html",
        {
            "request": request,
        },
    )


if not os.path.isdir("/tmp/scanandcut"):
    os.makedirs("/tmp/scanandcut")

routes = [
    Route("/", endpoint=home),
    Route("/edit/{path}/", endpoint=Edit),
    Route("/upload", endpoint=Upload),
    Mount(
        "/static",
        StaticFiles(
            directory=os.path.join(os.path.dirname(__file__), "static")
        ),
        name="static",
    ),
    Mount("/tmp", StaticFiles(directory=TMP_DIR), name="tmp"),
]

app = Starlette(debug=True, routes=routes)


def serve():
    sys.argv.insert(1, "scanandcut.web:app")
    uvicorn.main()


if __name__ == "__main__":
    serve()
